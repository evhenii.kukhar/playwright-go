export async function loginFunctionFromModule(page, loginUrl, loginEmail, loginPassword) {
  await page.goto(loginUrl);
  await page.getByRole('textbox').fill(loginEmail);
  await page.getByRole('textbox').press('Enter');
  await page.locator('input[name="password"]').fill(loginPassword);
  await page.locator('input[name="password"]').press('Enter');
}