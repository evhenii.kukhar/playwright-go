import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  use: {
    // Configure browser type; 'chromium' is default. You can also use 'firefox' or 'webkit'.
    browserName: 'chromium',

    // Launch headless by default. Headful can be enabled via `--headed` CLI flag or `headed: true` here.
    headless: true
  }
};

export default config;
